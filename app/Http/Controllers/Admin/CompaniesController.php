<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCompaniesRequest;
use App\Http\Requests\UpdateCompaniesRequest;
use App\Companies;
use Intervention\Image\ImageManagerStatic as Image;
use Session;

class CompaniesController extends Controller
{
    public function index()
    {
      
        $companies = Companies::paginate(10);

        return view('admin.companies.index', compact('companies'));
    }

    public function create()
    {
        
        return view('admin.companies.create');
    }

    public function store(StoreCompaniesRequest $request)
    {
       
        if ($request->hasFile('logo')) {
            $image = $request->file('logo');

            $image_extention = $image->getClientOriginalExtension();
            if($image_extention!='jpg' && $image_extention!='png' && $image_extention!='jpeg'){
                Session::flash('flash_message_error', 'Invalid Image.(Please select only jpg,jpeg or png file).');
                 return view('admin.companies.create');
            }else{
                $name = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = base_path('/public/companies_logo/');
                $imagePath = $destinationPath. "/".  $name;
                Image::make($image)->resize(750,750)->save($imagePath);
            }
        }else{
            Session::flash('flash_message_error', 'The logo is required');
            return view('admin.companies.create');
        }
        $input = $request->all();
        
        $Companies = new Companies;
        $Companies->name = $input['name'];
        $Companies->email = $input['email']; 
        $Companies->logo = $name;
        $Companies->save();
        

        return redirect()->route('admin.companies.index');
    }

    public function edit(Companies $companies, $id)
    {
       

        $companies = Companies::where('id', $id)->first();

        
        return view('admin.companies.edit', compact('companies'));
    }

    public function update(UpdateCompaniesRequest $request, Companies $companie, $id)
    {
        
        
        $companie->where('id', $id)->update(['name' => $request->name,'email'=>$request->email,'logo'=>$request->logo]);
      

        return redirect()->route('admin.companies.index');
    }

    public function destroy(Companies $Companie,$id)
    {
       
        $Companie=Companies::find($id);
        $Companie->delete();

        return back();
    }
}
