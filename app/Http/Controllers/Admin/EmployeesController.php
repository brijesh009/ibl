<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreEmployeesRequest; 
use App\Http\Requests\UpdateEmployeesRequest;
use App\Employees;
use App\Companies;

class EmployeesController extends Controller
{
    public function index()
    {
        $employees = Employees::paginate(10);
        
        return view('admin.employees.index', compact('employees'));
    }

    public function create()
    {
        
        $companies = Companies::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.employees.create',compact('companies'));
    }

    public function store(StoreEmployeesRequest $request)
    {
       
        $user = Employees::create($request->all());
        return redirect()->route('admin.employees.index');
    }

    public function edit(Employees $Employees, $id)
    {
     
        $companies = Companies::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $employees = Employees::where('id', $id)->first();

        
        return view('admin.employees.edit', compact('employees','companies'));
    }

    public function update(UpdateEmployeesRequest $request, Employees $employees, $id)
    {
        
        $employees->where('id', $id)->update(['first_name' => $request->first_name,'last_name' => $request->last_name,'company' => $request->company,'email'=>$request->email,'phone'=>$request->phone]);
      

        return redirect()->route('admin.employees.index');
    }


    public function destroy(Employees $employees,$id)
    {
       
        $employees=Employees::find($id);
        $employees->delete();

        return back();
    }
}
