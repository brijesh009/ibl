<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;

class StoreEmployeesRequest extends FormRequest
{
    // public function authorize()
    // {
    //     return \Gate::allows('user_create');
    // }

    public function rules()
    {
        return [
            'first_name'       => [
                'required',
            ],
            'last_name'      => [
                'required',
            ],
        ];
    }
}
