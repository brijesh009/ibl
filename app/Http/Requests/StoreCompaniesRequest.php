<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;

class StoreCompaniesRequest extends FormRequest
{
    // public function authorize()
    // {
    //     return \Gate::allows('user_create');
    // }

    public function rules()
    {
        return [
            'name'       => [
                'required',
            ],
            'email'      => [
                'required',
            ],
            'logo'   => [
                'required',
            ],
        ];
    }
}
