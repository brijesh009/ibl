<?php

namespace App;

use App\Notifications\VerifyUserNotification;
use Carbon\Carbon;
use Hash;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;

class Companies extends Authenticatable
{
    use SoftDeletes, Notifiable;

    public $table = 'companies';

    

    protected $dates = [
        'updated_at',
        'created_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
        'email',
        'logo',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function getEmailVerifiedAtAttribute($value)
    {
        return $value ? Carbon::createFromFormat('Y-m-d H:i:s', $value)->format(config('panel.date_format') . ' ' . config('panel.time_format')) : null;
    }
    public function candidate()
    {
        return $this->belongsTo(Companies::class, 'candidate_id');
    }

    // public function setEmailVerifiedAtAttribute($value)
    // {
    //     $this->attributes['email_verified_at'] = $value ? Carbon::createFromFormat(config('panel.date_format') . ' ' . config('panel.time_format'), $value)->format('Y-m-d H:i:s') : null;
    // }

    // public function setPasswordAttribute($input)
    // {
    //     if ($input) {
    //         $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
    //     }
    // }

    // public function sendPasswordResetNotification($token)
    // {
    //     $this->notify(new ResetPassword($token));
    // }

    // public function roles()
    // {
    //     return $this->belongsToMany(Role::class);
    // }

    // public function country()
    // {
    //     return $this->belongsTo(Country::class, 'country_id');
    // }

    // public function isEmployer()
    // {
    //     return $this->roles()->where('id', 2)->count() > 0;
    // }

    // public function isCandidate()
    // {
    //     return $this->roles()->where('id', 3)->count() > 0;
    // }

}
