<aside class="main-sidebar">
    <section class="sidebar" style="height: auto;">
        <ul class="sidebar-menu tree" data-widget="tree">
            @can('user_management_access')
                <li class="treeview">
                    <a>
                        <i class="fas fa-users">

                        </i>
                        <span>{{ trans('global.userManagement.title') }}</span>
                        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        
                        @can('role_access')
                            <li class="{{ request()->is('admin/employees') || request()->is('admin/employees/*') ? 'active' : '' }}">
                                <a href="{{ route("admin.employees.index") }}">
                                    <i class="fas fa-briefcase">

                                    </i>
                                    <span>{{ trans('global.employee.title') }}</span>
                                </a>
                            </li>
                        @endcan
                        @can('user_access')
                            <li class="{{ request()->is('admin/companies') || request()->is('admin/companies/*') ? 'active' : '' }}">
                                <a href="{{ route("admin.companies.index") }}">
                                    <i class="fas fa-user">

                                    </i>
                                    <span>{{ trans('global.companie.title') }}</span>
                                </a>
                            </li>
                        @endcan
                    </ul>
                </li>
                <li>
                <a href="#" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                    <i class="fas fa-sign-out-alt">

                    </i>
                    {{ trans('global.logout') }}
                </a>
            </li>
            @endcan
        </ul>
    </section>
</aside>