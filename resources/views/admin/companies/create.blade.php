@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            @if(Session::has('flash_message_error'))
            <div class="alert alert-danger">
                <button class="close" data-close="alert"></button>
                <span> {!! session('flash_message_error')!!} </span>
            </div>
            @endif
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.create') }} {{ trans('global.companie.title_singular') }}
                </div>
                <div class="panel-body">
                    
                    <form action="{{ route("admin.companies.store") }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label for="name">{{ trans('global.companie.fields.name') }}*</label>
                            <input type="text" id="name" name="name" class="form-control" >
                            @if($errors->has('name'))
                                <p class="help-block">
                                    {{ $errors->first('name') }}
                                </p>
                            @endif
                            <p class="helper-block">
                                {{ trans('global.companie.fields.name_helper') }}
                            </p>
                        </div>
                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                            <label for="email">{{ trans('global.companie.fields.email') }}*</label>
                            <input type="email" id="email" name="email" class="form-control">
                            @if($errors->has('email'))
                                <p class="help-block">
                                    {{ $errors->first('email') }}
                                </p>
                            @endif
                            <p class="helper-block">
                                {{ trans('global.companie.fields.email_helper') }}
                            </p>
                        </div>
                        <div class="form-group {{ $errors->has('logo') ? 'has-error' : '' }}">
                            <label for="password">{{ trans('global.companie.fields.logo') }}</label>
                            <!-- <input type="text" id="logo" name="logo" class="form-control"> -->
                            <input type="file" name="logo" id="logo" class="upload-btn"/>
                            @if($errors->has('logo'))
                                <p class="help-block">
                                    {{ $errors->first('logo') }}
                                </p>
                            @endif
                            <p class="helper-block">
                                {{ trans('global.companie.fields.logo_helper') }}
                            </p>
                        </div>

                        <div>
                            <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
                        </div>
                    </form>

                </div>
            </div>

        </div>
    </div>
</div>
@endsection