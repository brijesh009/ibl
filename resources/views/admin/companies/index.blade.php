@extends('layouts.admin')
@section('content')
<div class="content">
    @can('user_create')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success" href="{{ route("admin.companies.create") }}">
                    {{ trans('global.add') }} {{ trans('global.companie.title_singular') }}
                </a>
            </div>
        </div>
    @endcan
    <div class="row">
        <div class="col-lg-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.companie.title_singular') }} {{ trans('global.list') }}
                </div>
                <div class="panel-body">

                    <div class="table-responsive">
                        <table class=" table table-bordered table-striped table-hover datatable" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>
                                        {{ trans('global.companie.fields.name') }}
                                    </th>
                                    <th>
                                        {{ trans('global.companie.fields.email') }}
                                    </th>
                                    <th>
                                        {{ trans('global.companie.fields.logo') }}
                                    </th>
                                     <th>
                                        {{ trans('global.companie.fields.action') }}
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($companies as $key => $companie)
                                    <tr>
                                         <td>
                                            {{ $companie->name ?? '' }}
                                        </td>
                                        <td>
                                            {{ $companie->email ?? '' }}
                                        </td>
                                        <td>
                                            <img src="{{ asset('/public/companies_logo') }}/{{ $companie->logo }}" width="50" class="img-fluid" alt=""> 
                                        </td>

                                        <td>
                                            @can('user_edit')
                                                <a class="btn btn-xs btn-info" href="{{ route('admin.companies.edit', $companie->id) }}">
                                                    {{ trans('global.edit') }}
                                                </a>
                                            @endcan
                                            @can('user_delete')
                                                <form action="{{ route('admin.companies.destroy', $companie->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                                </form>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div style="text-align: center;">
                            {{ $companies->links() }}
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>
@endsection