@extends('layouts.admin')
@section('content')
<div class="content">
    @can('user_create')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success" href="{{ route("admin.employees.create") }}">
                    {{ trans('global.add') }} {{ trans('global.employee.title_singular') }}
                </a>
            </div>
        </div>
    @endcan
    <div class="row">
        <div class="col-lg-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.employee.title_singular') }} {{ trans('global.list') }}
                </div>
                <div class="panel-body">

                    <div class="table-responsive">
                        <table class=" table table-bordered table-striped table-hover datatable" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>
                                        {{ trans('global.employee.fields.first_name') }}
                                    </th>
                                    <th>
                                        {{ trans('global.employee.fields.last_name') }}
                                    </th>
                                    <th>
                                        {{ trans('global.employee.fields.company') }}
                                    </th>
                                    <th>
                                        {{ trans('global.employee.fields.email') }}
                                    </th>
                                    <th>
                                        {{ trans('global.employee.fields.phone') }}
                                    </th>
                                     <th>
                                        {{ trans('global.employee.fields.action') }}
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($employees as $key => $employee)
                                    <tr>
                                         <td>
                                            {{ $employee->first_name ?? '' }}
                                        </td><td>
                                            {{ $employee->last_name ?? '' }}
                                        </td>
                                        <td>
                                            {{ $employee->companys->name ?? '' }}
                                        </td>
                                        <td>
                                            {{ $employee->email ?? '' }}
                                        </td>
                                        <td>
                                            {{ $employee->phone ?? '' }}
                                        </td>

                                        <td>
                                           
                                            @can('user_edit')
                                                <a class="btn btn-xs btn-info" href="{{ route('admin.employees.edit', $employee->id) }}">
                                                    {{ trans('global.edit') }}
                                                </a>
                                            @endcan
                                            @can('user_delete')
                                                <form action="{{ route('admin.employees.destroy', $employee->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                                </form>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div style="text-align: center;">
                            {{ $employees->links() }}
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>
@endsection