@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.edit') }} {{ trans('global.employee.title_singular') }}
                </div>
                <div class="panel-body">

                    <form action="{{ route("admin.employees.update", [$employees->id]) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
                            <label for="first_name">{{ trans('global.employee.fields.first_name') }}*</label>
                            <input type="text" id="first_name" name="first_name" class="form-control" value="{{ old('first_name', isset($employees) ? $employees->first_name : '') }}">
                            @if($errors->has('first_name'))
                                <p class="help-block">
                                    {{ $errors->first('first_name') }}
                                </p>
                            @endif
                            <p class="helper-block">
                                {{ trans('global.employee.fields.first_name_helper') }}
                            </p>
                        </div>
                        <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
                            <label for="last_name">{{ trans('global.employee.fields.last_name') }}*</label>
                            <input type="text" id="last_name" name="last_name" class="form-control" value="{{ old('last_name', isset($employees) ? $employees->last_name : '') }}">
                            @if($errors->has('last_name'))
                                <p class="help-block">
                                    {{ $errors->first('last_name') }}
                                </p>
                            @endif
                            <p class="helper-block">
                                {{ trans('global.employee.fields.last_name_helper') }}
                            </p>
                        </div>
                        
                        <div class="form-group {{ $errors->has('company') ? 'has-error' : '' }}">
                            <label for="company">{{ trans('global.employee.fields.company') }}*</label>
                            <select name="company" id="company" class="form-control select2">
                                @foreach($companies as $id => $company)
                                    <option value="{{ $id }}" {{ (isset($employees) && $employees->company ? $employees->companys->id : old('company')) == $id ? 'selected' : '' }}>{{ $company }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('company'))
                                <p class="help-block">
                                    {{ $errors->first('company') }}
                                </p>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                            <label for="email">{{ trans('global.employee.fields.email') }}*</label>
                            <input type="email" id="email" name="email" class="form-control" value="{{ old('email', isset($employees) ? $employees->email : '') }}">
                            @if($errors->has('email'))
                                <p class="help-block">
                                    {{ $errors->first('email') }}
                                </p>
                            @endif
                            <p class="helper-block">
                                {{ trans('global.employee.fields.email_helper') }}
                            </p>
                        </div>
                        <div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
                            <label for="phone">{{ trans('global.employee.fields.phone') }}</label>
                            <input type="text" id="phone" name="phone" class="form-control" value="{{ old('email', isset($employees) ? $employees->phone : '') }}">
                            @if($errors->has('phone'))
                                <p class="help-block">
                                    {{ $errors->first('phone') }}
                                </p>
                            @endif
                            <p class="helper-block">
                                {{ trans('global.employee.fields.phone_helper') }}
                            </p>
                        </div>
                        <div>
                            <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
                        </div>
                    </form>

                </div>
            </div>

        </div>
    </div>
</div>
@endsection